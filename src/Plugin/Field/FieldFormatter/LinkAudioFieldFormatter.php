<?php

namespace Drupal\link_audio_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'Audio' formatter.
 *
 * @FieldFormatter(
 *   id = "link_audio_formatter",
 *   label = @Translation("Audio"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkAudioFieldFormatter extends LinkFormatter {

 /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'autoplay' => '0',
      'passthru' => '0',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autoplay enabled'),
      '#default_value' => $this->getSetting('autoplay'),
    ];

    $elements['passthru'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Render download links below player'),
      '#default_value' => $this->getSetting('passthru'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $settings = $this->getSettings();
    if ($settings['autoplay']) {
      $summary[] = $this->t('Autoplay is enabled.');
    }
    else {
      $summary[] = $this->t('Autoplay is not enabled.');
    }
    if ($settings['passthru']) {
      $summary[] = $this->t('Download links visible.');
    }
    else {
      $summary[] = $this->t('Download links hidden.');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // Render all field values as part of a single <audio> tag.
    $sources = [];
    foreach ($items as $item) {
      // Get the mime type.
      $mimetype = \Drupal::service('file.mime_type.guesser')->guess($item->uri);
      $sources[] = [
        'src' => $item->uri,
        'mimetype' => $mimetype,
      ];
    }

    // Configuration.
    $autoplay = '';
    if ($this->getSetting('autoplay')) {
      $autoplay = 'autoplay';
    }

    // Create render array for theming.
    $elements[] = [
      '#theme' => 'audio_tag',
      '#sources' => $sources,
      '#autoplay' => $autoplay,
    ];

    if ($this->getSetting('passthru')) {
      $elements = array_merge($elements, parent::viewElements($items, $langcode));
    }

    return $elements;
  }

}
