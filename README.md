# Link Audio Formatter
Link Audio field formatter module for Drupal 8+.

## Usage

1. Create a link field. May have a cardinality of 1 or more.
2. Set the "Audio" formatter under "Manage Display".
3. Create a node and link to one or more sound files using your field.
4. View your node, the HTML5 widget should appear with the list of sources in the same order they were linked to (OGG, MP3, etc.).

## Original Author
This project was forked from Michael Anello's [HTML5 Audio](https://github.com/ultimike/html5_audio) project.
